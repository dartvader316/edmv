#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/limits.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char* argv[]){
	if(argc<2){
		// Usage
		printf("edmv - rename/move your files in your $EDITOR \n");
		printf("Usage: \n");
		printf("\t%s [anyfile1] [anyfile2] ... \n", argv[0]);
		printf("Also works with shell preprocessing: \n");
		printf("\tSelect all files: %s * \n",argv[0]);
		printf("\tSelect file and file.bak: %s file{,.bak} \n",argv[0]);
		printf("\tetc...\n");

		return EXIT_FAILURE;
	}

	const char* editor = getenv("EDITOR");
	const char* tmpfile = "/tmp/.edmv";
	const char* ed_argv[] = {editor,tmpfile,NULL};
	
	FILE* fp = fopen(tmpfile,"w");
	if(fp){
		for(int i = 1;i<argc;i++){
			fprintf(fp,"%s\n",argv[i]);
		}
	}
	fclose(fp);
	
	// Open $EDITOR and wait for its PID
	int pid = fork();
	if(!pid){ 
		execvp(editor,(char* const*)ed_argv);
	}
	waitpid(pid,NULL,0);

	fp = fopen(tmpfile,"r");
	char** new_names = calloc(sizeof(char*),argc);
	
	int nc = 0;
	for(;nc<argc;nc++){
		new_names[nc] = calloc(sizeof(char),NAME_MAX);
		if(!fgets(new_names[nc],NAME_MAX,fp)){
			free(new_names[nc]);
			break;
		}else{ 
			const size_t len = strnlen(new_names[nc],NAME_MAX);
			new_names[nc][len-1] = '\0'; // Removes \n from the end
		}
	}

	if(nc == argc-1){
		for(int i = 0;i<nc;i++){
			rename(argv[i+1],new_names[i]);
			free(new_names[i]);
		}
	}else{
		puts("USER LOGIC ERROR: number of files after edit is wrong.");
		return EXIT_FAILURE;
	}

	fclose(fp);
	free(new_names);

	return EXIT_SUCCESS;
}
